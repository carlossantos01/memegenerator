const titleMeme = document.querySelector("#titleMeme");
const freeText = document.querySelector("#txtFreePhrase");
const firstOption1 = document.querySelector("#rdFirstPhrase1");
const firstOption2 = document.querySelector("#rdFirstPhrase2");
const secondOption1 = document.querySelector("#rdSecondPhrase1");
const secondOption2 = document.querySelector("#rdSecondPhrase2");
const secondOption3 = document.querySelector("#rdSecondPhrase3");
const imgMeme = document.querySelector(".imgMeme");
const slcImg = document.querySelector("#slcImg");
let firstPart = "";
let secondPart = "";
verifyImg();

function verifyText(){
    if(firstOption1.checked === true){
        firstPart = "Eis que";
    }else if(firstOption2.checked === true){
        firstPart = "Quando";
    }

    if(secondOption1.checked === true){
        secondPart = "o Irineu";
    }else if(secondOption2.checked === true){
        secondPart = "a 10/10"
    }else if(secondOption3.checked === true){
        secondPart = "você"
    }
}
function verifyImg(){
    if(slcImg.value === "imgOption1"){
        imgMeme.src = "https://memegenerator.net/img/images/17237120.jpg";
    }else if(slcImg.value === "imgOption2"){
        imgMeme.src = "https://pm1.narvii.com/6457/9fa0e724b7a9819f93d089ad3995afd98f6a7244_00.jpg";
    }else if(slcImg.value === "imgOption3"){
        imgMeme.src = "https://www.hojemais.com.br/imagem/noticia/1000/1000/411994_204644_15833.jpg";
    }
}
function modifyText(){
    verifyText();
    titleMeme.innerHTML = `${firstPart} ${secondPart} ${freeText.value}`;
}